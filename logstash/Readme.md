## Configuring ElasticSearch with (MariaDB)MySQL using Logstash: 

To understand more about CakePHP + ElasticSearch : https://book.cakephp.org/elasticsearch/3/en/index.html

1. ElasticSearch works with ODM rather than ORM. So we will need to convert the rquired tables (to be searched) into documents.
2. To create document from existing tables we will use *mariadb-java-client-2.6.1.jar*  along with Logstash to update the documents periodically.

### ElasticSearch users

To change user password while setting up for first time, execute 


    // To get inside elasticsearch container
    docker exec -it cdlidev_elasticsearch_1 sh

    // To setup new password for default users 
    elasticsearch-setup-passwords interactive

1. elastic:elasticchangeme
2. apm_system:elasticchangeme
3. kibana:elasticchangeme
4. kibana_system:elasticchangeme
5. logstash_system:elasticchangeme
6. beats_system:elasticchangeme
7. remote_monitoring_user:elasticchangeme

Reference : [Built-in users](https://www.elastic.co/guide/en/elasticsearch/reference/current/built-in-users.html)

## Required Files

- /logstash
    - /config
        - logstash.yml
    - /drivers
        - mariadb-java-client-2.6.1.jar
    - /pipeline
        - logstash.conf

## Reference:

1. [Configuring Logstash for Docker](https://www.elastic.co/guide/en/logstash/current/docker-config.html)
2. [How to keep Elasticsearch synchronized with a relational database using Logstash and JDBC](https://www.elastic.co/blog/how-to-keep-elasticsearch-synchronized-with-a-relational-database-using-logstash)
3. [Logstash input JDBC plugin and MariaDB](https://discuss.elastic.co/t/logstash-input-jdbc-plugin-and-mariadb/186985)
4. [Migrating MySql Data Into Elasticsearch Using Logstash](https://qbox.io/blog/migrating-mysql-data-into-elasticsearch-using-logstash)
5. [How elasticsearch will fetch data from MySQL database?](https://discuss.elastic.co/t/how-elasticsearch-will-fetch-data-from-mysql-database/139951)
6. [Configuring Security in Logstash](https://www.elastic.co/guide/en/logstash/7.8/ls-security.html)
7. [Basic ElasticSearch Query Example](https://coralogix.com/log-analytics-blog/42-elasticsearch-query-examples-hands-on-tutorial/)

## ElasticSearch Errors

1. Exit Code 137

Reference : https://github.com/moby/moby/issues/22211