<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class SearchController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'search']);
    }

    /**
     * beforeFilter method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Search Result View Settings
        $this->settings = [
            'LayoutType' => 1,
            'Page' => 1,
            'PageSize' => 10,
            'lastPage' => 0,
            'canViewPrivateArtifacts' => $this->GeneralFunctions->checkIfRolesExists([1, 4]) == 1 ? 1 : 0
        ];
    }

    /**
     * elasticSearchCredentials method
     *
     * @return array [username, password]
     */
    private function elasticSearchCredentials()
    {
        $username = 'elastic';
        $password = 'elasticchangeme';
        return [$username, $password];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Get Search Parameters $category => $wordToBeSearched
        $searchParam = isset($this->request->params['?']) ? $this->request->params['?'] : null;

        // If param is present
        if (isset($searchParam)) {
            $paramKeyValue = [];

            // Convert to array of params.
            foreach ($searchParam as $key => $value) {
                if (array_key_exists($key, $this->settings)) {
                    if ($key === 'LayoutType') {
                        $this->settings['LayoutType'] = in_array($value, [1, 2]) ? $value : 1;
                    } elseif ($key === 'PageSize') {
                        $this->settings['PageSize'] = ($value < 10) ? 10 : (($value < 25) ? 10 : (($value < 100) ? 25 : 100)) ;

                        // if the page size changes
                        $prevURL = parse_url($this->referer());
                        if (isset($prevURL['query'])) {
                            parse_str($prevURL['query'], $queryParsed);
                            if (!isset($queryParsed['PageSize']) || $queryParsed['PageSize'] != $value) {
                                $this->settings['Page'] = -1;
                            }
                        }
                    } elseif ($key === 'Page') {
                        $this->settings['Page'] = $this->settings['Page'] == -1 ? 1 : ($value > 0 ? $value : 1);
                    }
                } else {
                    array_push($paramKeyValue, [$key, trim($value, '" || \'')]);
                }
            }

            if ($this->settings['Page'] == -1) {
                $this->settings['Page'] = 1;
            }

            if ($paramKeyValue[0][1] !== '') {
                // Get Search Result
                $result = $this->search($paramKeyValue[0][0], $paramKeyValue[0][1]);
                $this->Flash->success(__("Showing ".sizeof($result)." search results."));

                $this->set([
                    'result' => $result,
                    'LayoutType' => $this->settings['LayoutType'],
                    'PageSize' => $this->settings['PageSize'],
                    'Page' => $this->settings['Page'],
                    'lastPage' => $this->settings['lastPage']
                ]);
            } else {
                $this->redirect('/search');
            }
        } else {
            $this->redirect('/');
        }
    }

    /**
     * Search method
     *
     * @param
     * searchCategory : Type of Simple Search
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function search($searchCategory, $keyword)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 300);

        $result = [];
        
        /**
         * Each artifact in search result will contain following keys
         * Data-Fields (9):
         * {
         *  Artifact => 1
         *  Author => 2
         *  Collection => 3
         *  Inscription => 4
         *  Material => 5
         *  Object Type => 6
         *  Period => 7
         *  Provenience => 8
         *  Publications => 9
         * }
         */

        // If searchCategory is 'Keyword'.
        if ($searchCategory === 'Keyword') {
            // Yet to be done.
        }

        // If searchCategory is 'Publication'.
        if ($searchCategory === 'Publication') {
            $result = $this->searchPublicationQuery($keyword);
        }

        // If searchCategory is 'Collection'.
        if ($searchCategory === 'Collection') {
            $result = $this->searchCollectionQuery($keyword);
        }

        // If searchCategory is 'Provenience'.
        if ($searchCategory === 'Provenience') {
            $result = $this->searchProvenienceQuery($keyword);
        }

        // If searchCategory is 'Period'.
        if ($searchCategory === 'Period') {
            $result = $this->searchPeriodQuery($keyword);
        }

        // If searchCategory is 'Transliteraton'.
        if ($searchCategory === 'Inscription') {
            $result = $this->searchInscriptionQuery($keyword);
        }

        // If searchCategory is 'ID'.
        if ($searchCategory === 'ID') {
            // Yet to be done.
        }

        // To add remaining fields to search result
        if (in_array($searchCategory, ['Collection', 'Inscription', 'Publication'])) {
            foreach ($result as $artifactId => $details) {
                $result[$artifactId] = array_merge(
                    $result[$artifactId],
                    $this->getArtifactWithId($artifactId)
                );
            }
        }

        return $result;
    }

    /**
     * searchPublicationQuery method
     *
     * To search for search-category Publication.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function searchPublicationQuery($keyword)
    {
        $from = $this->settings['Page'] == 1 ? 0 : ($this->settings['Page'] - 1)*$this->settings['PageSize'];

        if (!$this->settings['canViewPrivateArtifacts']) {
            $keyword .= ' AND is_public:true';
        }

        $data = '
            {
                "query": {
                    "query_string": {
                        "query": "'.$keyword.'",
                        "fields": ["exact_reference", "designation", "bibtexkey", "year", "publisher", "school", "series", "title", "book_title", "chapter", "journals_publications", "author_authors", "editor_authors"]
                    }
                },
                "collapse": {
                    "field": "artifact_id"     
                },
                "aggs": {
                    "total": {
                        "cardinality": {
                            "field": "artifact_id"
                        }
                    }
                },
                "sort": [
                            {
                            "_score": {
                                "order": "desc"
                            }
                    }
                ],
                "from": '.$from.',
                "size": '.$this->settings['PageSize'].'
            }
        ';

        $resultArray = $this->getDataFromES('artifacts_publications', $data);

        $this->settings['lastPage'] = ceil($resultArray['total'] / $this->settings['PageSize']);

        $resultArray = $resultArray['hits'];

        $result = [];

        foreach ($resultArray as $resultRow) {
            $result[$resultRow['artifact_id']] = [
                'author' => [
                    'id' => $resultRow['authors_id'],
                    'authors' => $resultRow['author_authors']
                ],
                'collection' => $this->getCollectionWithArtifactId($resultRow['artifact_id']),
                'inscription' => $this->getInscriptionWithArtifactId($resultRow['artifact_id']),
                'publication' => [
                    'publication_id' => $resultRow['publication_id'],
                    'year' => $resultRow['year'],
                ]
            ];
        }

        return $result;
    }

    /**
     * searchCollectionQuery method
     *
     * To search for search-category Collection.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function searchCollectionQuery($keyword)
    {
        $from = $this->settings['Page'] == 1 ? 0 : ($this->settings['Page'] - 1)*$this->settings['PageSize'];

        if (!$this->settings['canViewPrivateArtifacts']) {
            $keyword .= ' AND is_public:true';
        }

        $data = '
            {
                "query": {
                    "query_string": {
                        "query": "'.$keyword.'",
                        "fields": ["collection"]
                    }
                },
                "collapse": {
                    "field": "artifact_id"     
                },
                "aggs": {
                    "total": {
                        "cardinality": {
                            "field": "artifact_id"
                        }
                    }
                },
                "sort": [
                    {
                        "_score": {
                            "order": "desc"
                        }
                    }
                ],
                "from": '.$from.',
                "size": '.$this->settings['PageSize'].'
            }
        ';
        
        $resultArray = $this->getDataFromES('collection', $data);

        $this->settings['lastPage'] = ceil($resultArray['total'] / $this->settings['PageSize']);

        $resultArray = $resultArray['hits'];

        $result = [];

        foreach ($resultArray as $resultRow) {
            $result[$resultRow['artifact_id']] = [
                'collection' => [
                    'collection_id' => $resultRow['collection_id'],
                    'collection' => $resultRow['collection'],
                    'slug' => $resultRow['slug']
                ],
                'inscription' => $this->getInscriptionWithArtifactId($resultRow['artifact_id'])
            ];

            $result[$resultRow['artifact_id']] = array_merge(
                $result[$resultRow['artifact_id']],
                $this->getPublicationWithArtifactId($resultRow['artifact_id'])
            );
        }

        return $result;
    }

    /**
     * searchPeriodQuery method
     *
     * To search for search-category Period.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function searchPeriodQuery($keyword)
    {
        $from = $this->settings['Page'] == 1 ? 0 : ($this->settings['Page'] - 1)*$this->settings['PageSize'];

        if (!$this->settings['canViewPrivateArtifacts']) {
            $keyword .= ' AND is_public:true';
        }

        $data = '
            {
                "query": {
                    "query_string": {
                        "query": "'.$keyword.'",
                        "fields": ["period"]
                    }
                },
                "sort": [
                    {
                        "_score": {
                            "order": "desc"
                        }
                    }
                ],
                "from": '.$from.',
                "size": '.$this->settings['PageSize'].'
            }
        ';
            
        $resultArray = $this->getDataFromES('artifact', $data);
        
        $this->settings['lastPage'] = ceil($resultArray['total'] / $this->settings['PageSize']);

        $resultArray = $resultArray['hits'];

        $result = [];

        foreach ($resultArray as $resultRow) {
            $result[$resultRow['id']] = [
                'artifact' => [
                    'designation' => $resultRow['designation']
                ],
                'collection' => $this->getCollectionWithArtifactId($resultRow['id']),
                'inscription' => $this->getInscriptionWithArtifactId($resultRow['id']),
                'material' => [
                    'id' => $resultRow['materials_id'],
                    'material' => $resultRow['materials'],
                ],
                'object_type' => [
                    'id' => $resultRow['artifact_type_id'],
                    'artifact_type' => $resultRow['artifact_type'],
                ],
                'period' => [
                    'period_id' => $resultRow['period_id'],
                    'period' => $resultRow['period'],
                ],
                'provenience' => [
                    'provenience_id' => $resultRow['provenience_id'],
                    'provenience' => $resultRow['provenience'],
                ]
            ];

            $result[$resultRow['id']] = array_merge(
                $result[$resultRow['id']],
                $this->getPublicationWithArtifactId($resultRow['id'])
            );
        }

        return $result;
    }

    /**
     * searchProvenienceQuery method
     *
     * To search for search-category Provenience.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function searchProvenienceQuery($keyword)
    {
        $from = $this->settings['Page'] == 1 ? 0 : ($this->settings['Page'] - 1)*$this->settings['PageSize'];

        if (!$this->settings['canViewPrivateArtifacts']) {
            $keyword .= ' AND is_public:true';
        }

        $data = '
            {
                "query": {
                    "query_string": {
                        "query": "'.$keyword.'",
                        "fields": ["written_in", "provenience", "region"]
                    }
                },
                "sort": [
                    {
                        "_score": {
                            "order": "desc"
                        }
                    }
                ],
                "from": '.$from.',
                "size": '.$this->settings['PageSize'].'
            }
        ';
            
        $resultArray = $this->getDataFromES('artifact', $data);

        $this->settings['lastPage'] = ceil($resultArray['total'] / $this->settings['PageSize']);

        $resultArray = $resultArray['hits'];

        $result = [];

        foreach ($resultArray as $resultRow) {
            $result[$resultRow['id']] = [
                'artifact' => [
                    'designation' => $resultRow['designation']
                ],
                'period' => [
                    'period_id' => $resultRow['period_id'],
                    'period' => $resultRow['period'],
                ],
                'provenience' => [
                    'provenience_id' => $resultRow['provenience_id'],
                    'provenience' => $resultRow['provenience'],
                ],
                'object_type' => [
                    'id' => $resultRow['artifact_type_id'],
                    'artifact_type' => $resultRow['artifact_type'],
                ],
                'material' => [
                    'id' => $resultRow['materials_id'],
                    'material' => $resultRow['materials'],
                ],
                'collection' => $this->getCollectionWithArtifactId($resultRow['id']),
                'inscription' => $this->getInscriptionWithArtifactId($resultRow['id'])
            ];

            $result[$resultRow['id']] = array_merge(
                $result[$resultRow['id']],
                $this->getPublicationWithArtifactId($resultRow['id'])
            );
        }

        return $result;
    }

    /**
     * searchInscriptionQuery method
     *
     * To search for search-category Transliteraton.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     * keyword : input to be searched
     *
     * @return array of search result
     */
    public function searchInscriptionQuery($keyword)
    {
        $from = $this->settings['Page'] == 1 ? 0 : ($this->settings['Page'] - 1)*$this->settings['PageSize'];

        if (!$this->settings['canViewPrivateArtifacts']) {
            $keyword .= ' AND is_public:true';
        }

        $data = '
            {
                "query": {
                    "query_string": {
                        "query": "'.$keyword.'",
                        "fields": ["atf", "translation"]
                    }
                },
                "collapse": {
                    "field": "artifact_id" 
                },
                "aggs": {
                    "total": {
                        "cardinality": {
                            "field": "artifact_id"
                        }
                    }
                },
                "sort": [
                    {
                        "_score": {
                            "order": "desc"
                        }
                    }
                ],
                "from": '.$from.',
                "size": '.$this->settings['PageSize'].'
            }
        ';
            
        $resultArray = $this->getDataFromES('inscription', $data);

        $this->settings['lastPage'] = ceil($resultArray['total'] / $this->settings['PageSize']);

        $resultArray = $resultArray['hits'];

        $result = [];

        foreach ($resultArray as $resultRow) {
            $result[$resultRow['artifact_id']] = [
                'collection' => $this->getCollectionWithArtifactId($resultRow['artifact_id']),
                'inscription' => [
                    'id' => $resultRow['id'],
                    'atf' => $resultRow['atf'],
                    'translation' => $resultRow['translation']
                ]
            ];

            $result[$resultRow['artifact_id']] = array_merge(
                $result[$resultRow['artifact_id']],
                $this->getPublicationWithArtifactId($resultRow['artifact_id'])
            );
        }

        return $result;
    }

    /**
     * getArtifactWithId method
     *
     * To get artifact for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Artifact for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getArtifactWithId($artifactId)
    {
        $data =  '
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "id": '.$artifactId.'
                            }
                        }
                    ],
                    "boost" : 1.0
                }
            }
        }
        ';

        $resultArray = $this->getDataFromES('artifact', $data);
        $resultArray = $resultArray["hits"];
        $result = [];

        if (!empty($resultArray)) {
            $artifact = $resultArray[0];

            $result = [
                'artifact' => [
                    'designation' => $artifact['designation']
                ],
                'material' => [
                    'id' => $artifact['materials_id'],
                    'material' => $artifact['materials'],
                ],
                'object_type' => [
                    'id' => $artifact['artifact_type_id'],
                    'artifact_type' => $artifact['artifact_type'],
                ],
                'period' => [
                    'period_id' => $artifact['period_id'],
                    'period' => $artifact['period']
                ],
                'provenience' => [
                    'provenience_id' => $artifact['provenience_id'],
                    'provenience' => $artifact['provenience']
                ]
            ];
        }

        return $result;
    }

    /**
     * getInscriptionWithArtifactId method
     *
     * To get latest Inscription for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Inscription for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getInscriptionWithArtifactId($artifactId)
    {
        $data =  '
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "artifact_id": '.$artifactId.'
                            }
                        },
                        {
                            "term": {
                                "is_latest": true
                            }
                        }
                    ]
                }
            }
        }
        ';

        $resultArray = $this->getDataFromES('inscription', $data);

        $result = [];

        if (!empty($resultArray['hits'])) {
            $inscription = $resultArray['hits'][0];
            $result = [
                'id' => $inscription['id'],
                'atf' => $inscription['atf'],
                'translation' => $inscription['translation']
            ];
        }

        return $result;
    }

    /**
     * getCollectionWithArtifactId method
     *
     * To get Collection for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Collection for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getCollectionWithArtifactId($artifactId)
    {
        $data =  '
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "artifact_id": '.$artifactId.'
                            }
                        }
                    ]
                }
            }
        }
        ';

        $resultArray = $this->getDataFromES('collection', $data);
        
        $resultArray = $resultArray['hits'];
        
        $result = [];
        
        if (!empty($resultArray)) {
            $collection = $resultArray[0];
            $result = [
                'collection_id' => $collection['collection_id'],
                'collection' => $collection['collection'],
                'slug' => $collection['slug']
            ];
        }

        return $result;
    }

    /**
     * getPublicationWithArtifactId method
     *
     * To get Publication for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Publication for specific artifact ID.
     */
    public function getPublicationWithArtifactId($artifactId)
    {
        $data =  '
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "artifact_id": '.$artifactId.'
                            }
                        },
                        {
                            "term": {
                                "accepted": false 
                            }
                        }
                    ]
                }
            }
        }
        ';

        $resultArray = $this->getDataFromES('artifacts_publications', $data);

        $resultArray = $resultArray['hits'];

        $result = [];

        if (!empty($resultArray)) {
            $publication = $resultArray[0];
            $result = [
                'publication' => [
                    'publication_id' => $publication['publication_id'],
                    'year' => $publication['year']
                ],
                'author' => [
                    'id' => $publication['authors_id'],
                    'authors' => $publication['author_authors']
                ]
            ];
        }

        return $result;
    }

    /**
     * getDataFromES method
     *
     * This functions fetched data for specific ElasticSearch query with required parameters using cURL from ElasticSearch Server.
     *
     * @param
     * esIndex : Elastic Search Index to be used
     * data : Query parameters with respective values
     *
     * @return array of total results count and array of results.
     */
    private function getDataFromES($esIndex, $data)
    {
        [$elasticUser, $elasticPassword] = $this->elasticSearchCredentials();

        $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search?';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => ["Content-type: application/json"],
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => $data
        ]);

        $result = curl_exec($curl);
        $result = json_decode($result, true);

        curl_close($curl);

        $resultArray = [
            'total' => isset($result['aggregations']) ?
                $result['aggregations']['total']['value'] :
                (
                    isset($result['hits']) ? $result['hits']['total']['value'] : 0
                ),
            'hits' => !empty($result['hits']['total']) ? array_column($result['hits']['hits'], '_source') : []
        ];

        return $resultArray;
    }
}
