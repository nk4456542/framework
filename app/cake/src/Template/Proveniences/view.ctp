<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($provenience->provenience) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Provenience') ?></th>
                    <td><?= h($provenience->provenience) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Region') ?></th>
                    <td><?= $provenience->has('region') ? $this->Html->link($provenience->region->region, ['controller' => 'Regions', 'action' => 'view', $provenience->region->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Geo Coordinates') ?></th>
                    <td><?= h($provenience->geo_coordinates) ?></td>
                </tr>
               <!--  <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($provenience->id) ?></td>
                </tr> -->
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Provenience'), ['action' => 'edit', $provenience->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Provenience'), ['action' => 'delete', $provenience->id], ['confirm' => __('Are you sure you want to delete # {0}?', $provenience->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Proveniences'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Provenience'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Archives'), ['controller' => 'Archives', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Archive'), ['controller' => 'Archives', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Dynasties'), ['controller' => 'Dynasties', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Dynasty'), ['controller' => 'Dynasties', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($provenience->archives)): ?>
        <div class="capital-heading"><?= __('No Related Archives') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Archives') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
<!--                 <th scope="col"><?= __('Id') ?></th>
 -->                <th scope="col"><?= __('Archive') ?></th>
                <th scope="col"><?= __('Provenience Id') ?></th>
<!--                 <th scope="col"><?= __('Actions') ?></th>
 -->            </thead>
            <tbody>
                <?php foreach ($provenience->archives as $archives): ?>
                <tr>
<!--                     <td><?= h($archives->id) ?></td>
 -->                    <td><?= h($archives->archive) ?></td>
                    <td><?= h($archives->provenience_id) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Archives', 'action' => 'view', $archives->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Archives', 'action' => 'edit', $archives->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Archives', 'action' => 'delete', $archives->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $archives->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($provenience->artifacts)): ?>
        <div class="capital-heading"><?= __('No Related Artifacts') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <!-- <th scope="col"><?= __('Id') ?></th> -->
                <th scope="col"><?= __('Ark No') ?></th>
                <th scope="col"><?= __('Credit Id') ?></th>
                <th scope="col"><?= __('Primary Publication Comments') ?></th>
                <th scope="col"><?= __('Cdli Collation') ?></th>
                <th scope="col"><?= __('Cdli Comments') ?></th>
                <th scope="col"><?= __('Composite No') ?></th>
                <th scope="col"><?= __('Condition Description') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Date Comments') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Dates Referenced') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Electronic Publication') ?></th>
                <th scope="col"><?= __('Elevation') ?></th>
                <th scope="col"><?= __('Excavation No') ?></th>
                <th scope="col"><?= __('Findspot Comments') ?></th>
                <th scope="col"><?= __('Findspot Square') ?></th>
                <th scope="col"><?= __('Height') ?></th>
                <th scope="col"><?= __('Join Information') ?></th>
                <th scope="col"><?= __('Museum No') ?></th>
                <th scope="col"><?= __('Artifact Preservation') ?></th>
                <th scope="col"><?= __('Is Public') ?></th>
                <th scope="col"><?= __('Is Atf Public') ?></th>
                <th scope="col"><?= __('Are Images Public') ?></th>
                <th scope="col"><?= __('Seal No') ?></th>
                <th scope="col"><?= __('Seal Information') ?></th>
                <th scope="col"><?= __('Stratigraphic Level') ?></th>
                <th scope="col"><?= __('Surface Preservation') ?></th>
                <th scope="col"><?= __('General Comments') ?></th>
                <th scope="col"><?= __('Thickness') ?></th>
                <th scope="col"><?= __('Width') ?></th>
                <th scope="col"><?= __('Provenience Id') ?></th>
                <th scope="col"><?= __('Period Id') ?></th>
                <th scope="col"><?= __('Is Provenience Uncertain') ?></th>
                <th scope="col"><?= __('Is Period Uncertain') ?></th>
                <th scope="col"><?= __('Artifact Type Id') ?></th>
                <th scope="col"><?= __('Accession No') ?></th>
                <th scope="col"><?= __('Accounting Period') ?></th>
                <th scope="col"><?= __('Alternative Years') ?></th>
                <th scope="col"><?= __('Dumb2') ?></th>
                <th scope="col"><?= __('Custom Designation') ?></th>
                <th scope="col"><?= __('Period Comments') ?></th>
                <th scope="col"><?= __('Provenience Comments') ?></th>
                <th scope="col"><?= __('Is School Text') ?></th>
                <th scope="col"><?= __('Written In') ?></th>
                <th scope="col"><?= __('Is Object Type Uncertain') ?></th>
                <th scope="col"><?= __('Archive Id') ?></th>
                <th scope="col"><?= __('Created By') ?></th>
                <th scope="col"><?= __('Db Source') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Translation Source') ?></th>
                <th scope="col"><?= __('Atf Up') ?></th>
                <th scope="col"><?= __('Atf Source') ?></th>
                <!-- <th scope="col"><?= __('Actions') ?></th> -->
            </thead>
            <tbody>
                <?php foreach ($provenience->artifacts as $artifacts): ?>
                <tr>
                    <!-- <td><?= h($artifacts->id) ?></td> -->
                    <td><?= h($artifacts->ark_no) ?></td>
                    <td><?= h($artifacts->credit_id) ?></td>
                    <td><?= h($artifacts->primary_publication_comments) ?></td>
                    <td><?= h($artifacts->cdli_collation) ?></td>
                    <td><?= h($artifacts->cdli_comments) ?></td>
                    <td><?= h($artifacts->composite_no) ?></td>
                    <td><?= h($artifacts->condition_description) ?></td>
                    <td><?= h($artifacts->created) ?></td>
                    <td><?= h($artifacts->date_comments) ?></td>
                    <td><?= h($artifacts->modified) ?></td>
                    <td><?= h($artifacts->dates_referenced) ?></td>
                    <td><?= h($artifacts->designation) ?></td>
                    <td><?= h($artifacts->electronic_publication) ?></td>
                    <td><?= h($artifacts->elevation) ?></td>
                    <td><?= h($artifacts->excavation_no) ?></td>
                    <td><?= h($artifacts->findspot_comments) ?></td>
                    <td><?= h($artifacts->findspot_square) ?></td>
                    <td><?= h($artifacts->height) ?></td>
                    <td><?= h($artifacts->join_information) ?></td>
                    <td><?= h($artifacts->museum_no) ?></td>
                    <td><?= h($artifacts->artifact_preservation) ?></td>
                    <td><?= h($artifacts->is_public) ?></td>
                    <td><?= h($artifacts->is_atf_public) ?></td>
                    <td><?= h($artifacts->are_images_public) ?></td>
                    <td><?= h($artifacts->seal_no) ?></td>
                    <td><?= h($artifacts->seal_information) ?></td>
                    <td><?= h($artifacts->stratigraphic_level) ?></td>
                    <td><?= h($artifacts->surface_preservation) ?></td>
                    <td><?= h($artifacts->general_comments) ?></td>
                    <td><?= h($artifacts->thickness) ?></td>
                    <td><?= h($artifacts->width) ?></td>
                    <td><?= h($artifacts->provenience_id) ?></td>
                    <td><?= h($artifacts->period_id) ?></td>
                    <td><?= h($artifacts->is_provenience_uncertain) ?></td>
                    <td><?= h($artifacts->is_period_uncertain) ?></td>
                    <td><?= h($artifacts->artifact_type_id) ?></td>
                    <td><?= h($artifacts->accession_no) ?></td>
                    <td><?= h($artifacts->accounting_period) ?></td>
                    <td><?= h($artifacts->alternative_years) ?></td>
                    <td><?= h($artifacts->dumb2) ?></td>
                    <td><?= h($artifacts->custom_designation) ?></td>
                    <td><?= h($artifacts->period_comments) ?></td>
                    <td><?= h($artifacts->provenience_comments) ?></td>
                    <td><?= h($artifacts->is_school_text) ?></td>
                    <td><?= h($artifacts->written_in) ?></td>
                    <td><?= h($artifacts->is_object_type_uncertain) ?></td>
                    <td><?= h($artifacts->archive_id) ?></td>
                    <td><?= h($artifacts->created_by) ?></td>
                    <td><?= h($artifacts->db_source) ?></td>
                    <td><?= h($artifacts->weight) ?></td>
                    <td><?= h($artifacts->translation_source) ?></td>
                    <td><?= h($artifacts->atf_up) ?></td>
                    <td><?= h($artifacts->atf_source) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Artifacts', 'action' => 'view', $artifacts->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Artifacts', 'action' => 'edit', $artifacts->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Artifacts', 'action' => 'delete', $artifacts->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $artifacts->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($provenience->dynasties)): ?>
        <div class="capital-heading"><?= __('No Related Dynasties') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Dynasties') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
                <!-- <th scope="col"><?= __('Id') ?></th> -->
                <th scope="col"><?= __('Polity') ?></th>
                <th scope="col"><?= __('Dynasty') ?></th>
                <th scope="col"><?= __('Sequence') ?></th>
                <th scope="col"><?= __('Provenience Id') ?></th>
                <!-- <th scope="col"><?= __('Actions') ?></th> -->
            </thead>
            <tbody>
                <?php foreach ($provenience->dynasties as $dynasties): ?>
                <tr>
                    <!-- <td><?= h($dynasties->id) ?></td> -->
                    <td><?= h($dynasties->polity) ?></td>
                    <td><?= h($dynasties->dynasty) ?></td>
                    <td><?= h($dynasties->sequence) ?></td>
                    <td><?= h($dynasties->provenience_id) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Dynasties', 'action' => 'view', $dynasties->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Dynasties', 'action' => 'edit', $dynasties->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Dynasties', 'action' => 'delete', $dynasties->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $dynasties->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


