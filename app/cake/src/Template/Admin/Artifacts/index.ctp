<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact[]|\Cake\Collection\CollectionInterface $artifacts
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Artifact'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Periods'), ['controller' => 'Periods', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Period'), ['controller' => 'Periods', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifact Types'), ['controller' => 'ArtifactTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact Type'), ['controller' => 'ArtifactTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Archives'), ['controller' => 'Archives', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Archive'), ['controller' => 'Archives', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Composites'), ['controller' => 'ArtifactsComposites', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Composite'), ['controller' => 'ArtifactsComposites', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Seals'), ['controller' => 'ArtifactsSeals', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Seal'), ['controller' => 'ArtifactsSeals', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Shadow'), ['controller' => 'ArtifactsShadow', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Shadow'), ['controller' => 'ArtifactsShadow', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Inscriptions'), ['controller' => 'Inscriptions', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Inscription'), ['controller' => 'Inscriptions', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Retired Artifacts'), ['controller' => 'RetiredArtifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Retired Artifact'), ['controller' => 'RetiredArtifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Dates'), ['controller' => 'Dates', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Date'), ['controller' => 'Dates', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List External Resources'), ['controller' => 'ExternalResources', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New External Resource'), ['controller' => 'ExternalResources', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Artifacts') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <!-- <th scope="col"><?= $this->Paginator->sort('id') ?></th> -->
            <th scope="col"><?= $this->Paginator->sort('ark_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('credit_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('primary_publication_comments') ?></th>
            <th scope="col"><?= $this->Paginator->sort('cdli_collation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('composite_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('condition_description') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('date_comments') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col"><?= $this->Paginator->sort('dates_referenced') ?></th>
            <th scope="col"><?= $this->Paginator->sort('designation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('electronic_publication') ?></th>
            <th scope="col"><?= $this->Paginator->sort('elevation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('excavation_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('findspot_square') ?></th>
            <th scope="col"><?= $this->Paginator->sort('height') ?></th>
            <th scope="col"><?= $this->Paginator->sort('join_information') ?></th>
            <th scope="col"><?= $this->Paginator->sort('museum_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_preservation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_atf_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('are_images_public') ?></th>
            <th scope="col"><?= $this->Paginator->sort('seal_no') ?></th>
            <th scope="col"><?= $this->Paginator->sort('stratigraphic_level') ?></th>
            <th scope="col"><?= $this->Paginator->sort('surface_preservation') ?></th>
            <th scope="col"><?= $this->Paginator->sort('thickness') ?></th>
            <th scope="col"><?= $this->Paginator->sort('width') ?></th>
            <th scope="col"><?= $this->Paginator->sort('provenience_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('period_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_provenience_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_period_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('artifact_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('accounting_period') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_school_text') ?></th>
            <th scope="col"><?= $this->Paginator->sort('written_in') ?></th>
            <th scope="col"><?= $this->Paginator->sort('is_object_type_uncertain') ?></th>
            <th scope="col"><?= $this->Paginator->sort('archive_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created_by') ?></th>
            <th scope="col"><?= $this->Paginator->sort('db_source') ?></th>
            <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
            <th scope="col"><?= $this->Paginator->sort('atf_source') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifacts as $artifact): ?>
        <tr>
            <!-- <td><= $this->Number->format($artifact->id) ?></td> -->
            <td><?= h($artifact->ark_no) ?></td>
            <td><?= $this->Number->format($artifact->credit_id) ?></td>
            <td><?= h($artifact->primary_publication_comments) ?></td>
            <td><?= h($artifact->cdli_collation) ?></td>
            <td><?= h($artifact->composite_no) ?></td>
            <td><?= h($artifact->condition_description) ?></td>
            <td><?= h($artifact->created) ?></td>
            <td><?= h($artifact->date_comments) ?></td>
            <td><?= h($artifact->modified) ?></td>
            <td><?= h($artifact->dates_referenced) ?></td>
            <td><?= h($artifact->designation) ?></td>
            <td><?= h($artifact->electronic_publication) ?></td>
            <td><?= h($artifact->elevation) ?></td>
            <td><?= h($artifact->excavation_no) ?></td>
            <td><?= h($artifact->findspot_square) ?></td>
            <td><?= $this->Number->format($artifact->height) ?></td>
            <td><?= h($artifact->join_information) ?></td>
            <td><?= h($artifact->museum_no) ?></td>
            <td><?= h($artifact->artifact_preservation) ?></td>
            <td><?= h($artifact->is_public) ?></td>
            <td><?= h($artifact->is_atf_public) ?></td>
            <td><?= h($artifact->are_images_public) ?></td>
            <td><?= h($artifact->seal_no) ?></td>
            <td><?= h($artifact->stratigraphic_level) ?></td>
            <td><?= h($artifact->surface_preservation) ?></td>
            <td><?= $this->Number->format($artifact->thickness) ?></td>
            <td><?= $this->Number->format($artifact->width) ?></td>
            <td><?= $artifact->has('provenience') ? $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) : '' ?></td>
            <td><?= $artifact->has('period') ? $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) : '' ?></td>
            <td><?= h($artifact->is_provenience_uncertain) ?></td>
            <td><?= h($artifact->is_period_uncertain) ?></td>
            <td><?= $artifact->has('artifact_type') ? $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) : '' ?></td>
            <td><?= $this->Number->format($artifact->accounting_period) ?></td>
            <td><?= h($artifact->is_school_text) ?></td>
            <td><?= $this->Number->format($artifact->written_in) ?></td>
            <td><?= $this->Number->format($artifact->is_object_type_uncertain) ?></td>
            <td><?= $artifact->has('archive') ? $this->Html->link($artifact->archive->archive, ['controller' => 'Archives', 'action' => 'view', $artifact->archive->id]) : '' ?></td>
            <td><?= h($artifact->created_by) ?></td>
            <td><?= h($artifact->db_source) ?></td>
            <td><?= $this->Number->format($artifact->weight) ?></td>
            <td><?= h($artifact->atf_source) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifact->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifact->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifact->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifact->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

