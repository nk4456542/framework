<h1 class="display-3 header-txt text-left"><?= __('Merge Publications') ?></h1>
<h3 class="display-4 pt-3 text-left"><?= __('Publications Selected For Merging') ?></h3>

<?= $this->Form->create($publication_merge, ['action' => 'merge', 'type' => 'post']) ?>
<div class="table-responsive">
    <table class="table-bootstrap my-3 mx-0">
        <thead>
            <tr>
                <th scope="col">BibTeX key</th>
                <th scope="col">Authors</th>
                <th scope="col">Title</th>
                <th scope="col">Journal</th>
                <th scope="col">Designation</th>
                <th scope="col">Reference</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($publications as $publication): ?>
            <tr>
                <?= // To pass back the list of ids for processing
                    $this->Form->hidden('ids[]', ['value' => $publication->id]) ?>
                <td>
                    <?= $this->Html->link(
                                h($publication->bibtexkey),
                                ['prefix' => false, 'action' => 'view', $publication->id]) ?>
                </td>
                <td align="left" nowrap="nowrap">
                    <?php foreach ($publication->authors as $author): ?>
                    <?= $this->Html->link(
                                h($author->author),
                                ['prefix' => false, 'controller' => 'Authors', 'action' => 'view', $author->id]) ?><br>
                    <?php endforeach ?>
                </td>
                <td align="left">
                    <?= (isset($publication->title) and !empty($publication->title)) ? '<b>Title: </b>'.h($publication->title) : '' ?><br>
                    <?= (isset($publication->book_title) and !empty($publication->book_title)) ? '<b>Book Title: </b>'.h($publication->book_title) : '' ?><br>
                    <?= (isset($publication->chapter) and !empty($publication->chapter)) ? '<b>Chapter: </b>'.h($publication->chapter) : '' ?>
                </td>
                <td align="left">
                    <?= isset($publication->journal->journal) ? '<b>Journal: </b>'.h($publication->journal->journal) : '' ?><br>
                    <?= (isset($publication->volume) and !empty($publication->volume)) ? '<b>Volume: </b>'.h($publication->book_title) : '' ?><br>
                    <?= (isset($publication->number) and !empty($publication->number)) ? '<b>Number: </b>'.h($publication->chapter) : '' ?>
                </td>
                <td><?= h($publication->designation) ?></td>
                <td>
                    <?= $this->Scripts->formatReference($publication, 'bibliography', [
                            'template' => 'chicago',
                            'format' => 'html'
                        ]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<h3 class="display-4 pt-3 text-left"><?= __('Merged Publication Details') ?></h3>
<div class="col-lg boxed mx-0">
    <div class="ads">
        <legend class="capital-heading"><?= __('Publication Data') ?></legend>
        <div class="layout-grid text-left">
            <div>
                Bibtex Key:
                <?php echo $this->Form->control('bibtexkey', ['label' => false, 'type' => 'text']); ?>
                Title:
                <?php echo $this->Form->control('title', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                Year:
                <?php echo $this->Form->control('year', ['label' => false, 'type' => 'text', 'maxLength' => 20]); ?>
                Entry Type:
                <?php echo $this->Form->control('entry_type_id', ['label' => false, 'options' => $entryTypes, 'empty' => true]);  ?>
                Address:
                <?php echo $this->Form->control('address', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                Annote:
                <?php echo $this->Form->control('annote', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                Book Title:
                <?php echo $this->Form->control('book_title', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                Chapter:
                <?php echo $this->Form->control('chapter', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                Cross Reference:
                <?php echo $this->Form->control('crossref', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                Edition:
                <?php echo $this->Form->control('edition', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                How Published:
                <?php echo $this->Form->control('how_published', ['label' => false, 'type' => 'text', 'maxLength' => 255]);  ?>
                Institution:
                <?php echo $this->Form->control('institution', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                Journal:
                <?php echo $this->Form->control('journal_id', ['label' => false, 'options' => $journals, 'empty' => true]);  ?>
                <?php
                    // Populating authors and editors values
                    $authors = [];
                    $editors = [];
                    if (isset($publication_merge->authors)) {
                        foreach ($publication_merge->authors as $author) {
                            array_push($authors, $author->author);
                        }
                    }
                    if (isset($publication_merge->editors)) {
                        foreach ($publication_merge->editors as $editor) {
                            array_push($editors, $editor->author);
                        }
                    }
                    $authors = implode(';', $authors);
                    $editors = implode(';', $editors);
                ?>
                Author List:
                <div class="form-group">
                    <input id="authors_input" name="authors" type="text" class="form-control"
                        value="<?= $authors ?>">
                    <div id="input-foot-authors-parent" class="input-foot-authors-parent"></div>
                </div>
            </div>

            <div>
                <div>
                    Month:
                    <?php echo $this->Form->control('month', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Note:
                    <?php echo $this->Form->control('note', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Number:
                    <?php echo $this->Form->control('number', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                    Organization:
                    <?php echo $this->Form->control('organization', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Pages:
                    <?php echo $this->Form->control('pages', ['label' => false, 'type' => 'text', 'maxLength' => 45]);  ?>
                    Publisher:
                    <?php echo $this->Form->control('publisher', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                    School:
                    <?php echo $this->Form->control('school', ['label' => false, 'type' => 'text', 'maxLength' => 80]);  ?>
                    Volume:
                    <?php echo $this->Form->control('volume', ['label' => false, 'type' => 'text', 'maxLength' => 50]);  ?>
                    Publication History:
                    <?php echo $this->Form->control('publication_history', ['label' => false, 'type' => 'text']);  ?>
                    Series:
                    <?php echo $this->Form->control('series', ['label' => false, 'type' => 'text', 'maxLength' => 100]);  ?>
                    OCLC:
                    <?php echo $this->Form->control('oclc', ['label' => false, 'type' => 'number']);  ?>
                    Designation:
                    <?php echo $this->Form->control('designation', ['label' => false, 'type' => 'text']); ?>
                    Editor List:
                    <div class="form-group">
                        <input id="editors_input" name="editors" type="text" class="form-control"
                            value="<?= $editors ?>">
                        <div id="input-foot-editors-parent" class="input-foot-editors-parent"></div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (empty($publication_merge->artifacts)): ?>
        <div class="capital-heading"><?= __('No Related Artifacts') ?></div>
        <?php else: ?>
        <legend class="capital-heading"><?= __('Linked Artifacts Data') ?></legend>
        <div class="table-responsive">
            <table class="table-bootstrap mx-0">
                <thead>
                    <tr>
                        <th scope="col">Artifact Information</th>
                        <th scope="col">Exact Reference</th>
                        <th scope="col">Publication Type</th>
                        <th scope="col">Publication Comments</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($publication_merge->artifacts as $key => $artifact): ?>
                    <?= $this->Form->input("artifacts.{$key}.id", ['type' => 'hidden', 'value' => $artifact->id]) ?>
                    <?= $this->Form->input("artifacts.{$key}._joinData.publication_id", ['type' => 'hidden', 'value' => $publication_merge->bibtexkey]) ?>
                    <?= $this->Form->input("artifacts.{$key}._joinData.artifact_id", ['type' => 'hidden', 'value' => $artifact->id]) ?>
                    <?= $this->Form->input("artifacts.{$key}._joinData.id", ['type' => 'hidden', 'value' => $artifact->_joinData->id]) ?>
                    <tr>
                        <td>
                            <ul>
                                <li><b><?= __('Artifact Identifier: ') ?></b><?= $this->Html->link(h('P'.substr("00000{$artifact->id}", -6)), ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]) ?>
                                </li>
                                <?php if (!empty($artifact->designation)): ?>
                                <li>
                                    <b><?= __('Designation: ') ?></b>
                                    <?= h($artifact->designation) ?>
                                </li>
                                <?php endif ?>
                                <?php if (!empty($artifact->collections)): ?>
                                <li>
                                    <b><?= __('Museum Collections: ') ?></b>
                                    <?php foreach ($artifact->collections as $i => $collection): ?>
                                    <?php if ($i != 0): ?>
                                    <?= '; ' ?>
                                    <?php endif ?>
                                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                                    <?php endforeach; ?>
                                </li>
                                <?php endif ?>
                                <?php if (!empty($artifact->period)): ?>
                                <li>
                                    <b><?= __('Period: ') ?></b>
                                    <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($artifact->provenience)): ?>
                                <li>
                                    <b><?= __('Provenience: ') ?></b>
                                    <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($artifact->artifact_type)): ?>
                                <li>
                                    <b><?= __('Artifact Type: ') ?></b>
                                    <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                                </li>
                                <?php endif; ?>
                                <?php if (!empty($artifact->artifact_type)): ?>
                                <li>
                                    <b><?= __('Accession: ') ?></b>
                                    <?= $this->Html->link($artifact->accession, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </td>
                        <td><?= $this->Form->input("artifacts.{$key}._joinData.exact_reference", ['label' => false, 'type' => 'text', 'value' => $artifact->_joinData->exact_reference]); ?>
                        </td>
                        <td><?= $this->Form->input("artifacts.{$key}._joinData.publication_type", ['label' => false, 'type' => 'select', 'options' => $publication_type_options, 'class' => 'form-select', 'value' => $artifact->_joinData->publication_type]); ?>
                        </td>
                        <td><?= $this->Form->input("artifacts.{$key}._joinData.publication_comments", ['label' => false, 'type' => 'textarea', 'value' => $artifact->_joinData->publication_comments]); ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
        <div class="ads-space-top"><?= $this->Form->submit('Submit', ['class' => 'btn cdli-btn-blue']) ?></div>
        <?= $this->Form->end() ?>
    </div>
</div>
</div>

<script src="/assets/js/autocomplete.js"></script>
