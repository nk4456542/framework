<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period $period
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($period) ?>
            <legend class="capital-heading"><?= __('Edit Period') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('period');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $period->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $period->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Periods'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Years'), ['controller' => 'Years', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Year'), ['controller' => 'Years', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
