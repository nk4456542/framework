<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsGenre $artifactsGenre
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsGenre) ?>
            <legend class="capital-heading"><?= __('Add Artifacts Genre') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('genre_id', ['options' => $genres, 'empty' => true]);
                echo $this->Form->control('is_genre_uncertain');
                echo $this->Form->control('comments');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Artifacts Genres'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Genres'), ['controller' => 'Genres', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Genre'), ['controller' => 'Genres', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
