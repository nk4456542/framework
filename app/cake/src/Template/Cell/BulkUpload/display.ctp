<?php if (isset($error_list) and !isset($entities)): ?>
    <h3 class="display-4 pt-3"><?= __('Error Report') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
        <thead>
        <tr>
            <th scope="col"><?= __('Line No.') ?></th>
            <th scope="col"><?= __('Data') ?></th> 
            <th scope="col"><?= __('Errors') ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($error_list as $error): ?>
            <tr>
                <td> <?= h($error[0]) ?></td>
                <td> <?= h($error[1]) ?></td>
                <td>
                    <?php foreach ($error[2] as $row): ?>
                        <?= h($row) ?><br>
                    <?php endforeach; ?>
                    <?php if(isset($error[3])): ?>
                            <?= $this->Html->link('(Edit Link)', ['controller' => $table, 'action' => 'edit', $error[3]], ['target' => '_blank']) ?>
                    <?php endif ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <?= $this->Html->link('Export', [
        'action' => 'export/', serialize($error_list), serialize($header),
        '_ext' => 'csv'
        ], ['class' => 'btn btn-primary']) ?>

    <?= $this->Html->link('Cancel', [
        'action' => 'add', 'bulk'],
        ['class' => 'btn btn-primary']) ?> 
<?php endif; ?>      

<div class="mx-0 boxed">
<div class="capital-heading"><?= __('Bulk Upload '.preg_replace('/(?<!^)[A-Z]/', '-$0', $table).' Links') ?></div>
        <?= $this->Form->create($table, ['type' => 'file','url' => ['controller'=>$table,'action' => 'add', 'bulk'], 'class'=>'form-inline', 'role'=>'form']) ?>
        <div>
            <label class="sr-only" for="csv"> CSV </label>
            <?php echo $this->Form->input('csv', ['type'=>'file','class' => 'form-control', 'label' => false, 'placeholder' => 'csv upload']); ?>
        </div>
        <?= $this->Form->submit('Submit',['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
