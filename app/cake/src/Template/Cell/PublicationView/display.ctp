<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="capital-heading">
            <?= __('Publication Information:') ?>
            <?= $this->Html->link('View Publication', [
                'prefix' => false,
                'controller' => 'Publications',
                'action' => 'view',
                $publication->id
            ], [
                'class' => 'btn btn-action',
                'style' => "float: right;"
            ]) ?>
        </div>
        <table class="table-bootstrap mx-0">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Designation') ?></th>
                    <td>
                        <?= h($publication->designation) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Reference') ?></th>
                    <td>
                        <?= $this->Scripts->formatReference($publication, 'bibliography', [
                                'template' => 'chicago',
                                'format' => 'html'
                            ]) ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Authors') ?></th>
                    <td>
                        <?php $authors = array() ?>
                        <?php foreach ($publication->authors as $author): ?>
                            <?php array_push($authors, $this->Html->link(
                                h($author->author),
                                ['controller' => 'Authors', 'action' => 'view', $author->id]) )  ?>
                        <?php endforeach; ?>
                        <?= implode($authors,'; '); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Editors') ?></th>
                    <td>
                        <?php $editors = array() ?>
                        <?php foreach ($publication->editors as $editor): ?>
                            <?php array_push($editors, $this->Html->link(
                                h($editor->author),
                                ['controller' => 'Authors', 'action' => 'view', $editor->id]) )  ?>
                        <?php endforeach; ?>
                        <?= implode($editors,'; '); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
