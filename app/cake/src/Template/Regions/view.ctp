<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Region $region
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($region->region) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Region') ?></th>
                    <td><?= h($region->region) ?></td>
                </tr>
                <!-- <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($region->id) ?></td>
                </tr> -->
                <tr>
                    <th scope="row"><?= __('Geo Coordinates') ?></th>
                    <td><?= $this->Text->autoParagraph(h($region->geo_coordinates)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Region'), ['action' => 'edit', $region->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Region'), ['action' => 'delete', $region->id], ['confirm' => __('Are you sure you want to delete # {0}?', $region->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Regions'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Region'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if (empty($region->proveniences)): ?>
        <div class="capital-heading"><?= __('No Related Proveniences') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Proveniences') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
<!--                 <th scope="col"><?= __('Id') ?></th>
 -->                <th scope="col"><?= __('Provenience') ?></th>
                <th scope="col"><?= __('Region Id') ?></th>
                <th scope="col"><?= __('Geo Coordinates') ?></th>
                <!-- <th scope="col"><?= __('Actions') ?></th> -->
            </thead>
            <tbody>
                <?php foreach ($region->proveniences as $proveniences): ?>
                <tr>
                    <!-- <td><?= h($proveniences->id) ?></td> -->
                    <td><?= h($proveniences->provenience) ?></td>
                    <td><?= h($proveniences->region_id) ?></td>
                    <td><?= h($proveniences->geo_coordinates) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Proveniences', 'action' => 'view', $proveniences->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Proveniences', 'action' => 'edit', $proveniences->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Proveniences', 'action' => 'delete', $proveniences->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $proveniences->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


