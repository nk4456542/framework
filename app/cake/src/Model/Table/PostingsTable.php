<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Postings Model
 *
 * @property \App\Model\Table\PostingTypesTable|\Cake\ORM\Association\BelongsTo $PostingTypes
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 *
 * @method \App\Model\Entity\Posting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Posting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Posting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Posting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Posting|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Posting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Posting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Posting findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('postings');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PostingTypes', [
            'foreignKey' => 'posting_type_id'
        ]);
        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->allowEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('published')
            ->allowEmpty('published');

        $validator
            ->scalar('title')
            ->allowEmpty('title');

        $validator
            ->scalar('body')
            ->maxLength('body', 16777215)
            ->allowEmpty('body');

        $validator
            ->scalar('lang')
            ->maxLength('lang', 3)
            ->allowEmpty('lang');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->scalar('modified_by')
            ->maxLength('modified_by', 45)
            ->allowEmpty('modified_by');

        $validator
            ->dateTime('publish_start')
            ->allowEmpty('publish_start');

        $validator
            ->dateTime('publish_end')
            ->allowEmpty('publish_end');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->isUnique(['slug']));
        $rules->add($rules->existsIn(['posting_type_id'], 'PostingTypes'));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));

        return $rules;
    }
}
