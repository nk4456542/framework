<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dates Model
 *
 * @property \App\Model\Table\MonthsTable|\Cake\ORM\Association\BelongsTo $Months
 * @property \App\Model\Table\YearsTable|\Cake\ORM\Association\BelongsTo $Years
 * @property \App\Model\Table\DynastiesTable|\Cake\ORM\Association\BelongsTo $Dynasties
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\BelongsTo $Rulers
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsToMany $Artifacts
 *
 * @method \App\Model\Entity\Date get($primaryKey, $options = [])
 * @method \App\Model\Entity\Date newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Date[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Date|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Date|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Date patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Date[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Date findOrCreate($search, callable $callback = null, $options = [])
 */
class DatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Months', [
            'foreignKey' => 'month_id'
        ]);
        $this->belongsTo('Years', [
            'foreignKey' => 'year_id'
        ]);
        $this->belongsTo('Dynasties', [
            'foreignKey' => 'dynasty_id'
        ]);
        $this->belongsTo('Rulers', [
            'foreignKey' => 'ruler_id'
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'date_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_dates'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('day_no')
            ->maxLength('day_no', 50)
            ->allowEmpty('day_no');

        $validator
            ->scalar('day_remarks')
            ->allowEmpty('day_remarks');

        $validator
            ->boolean('is_uncertain')
            ->allowEmpty('is_uncertain');

        $validator
            ->scalar('month_no')
            ->maxLength('month_no', 50)
            ->allowEmpty('month_no');

        $validator
            ->scalar('absolute_year')
            ->maxLength('absolute_year', 15)
            ->allowEmpty('absolute_year');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['month_id'], 'Months'));
        $rules->add($rules->existsIn(['year_id'], 'Years'));
        $rules->add($rules->existsIn(['dynasty_id'], 'Dynasties'));
        $rules->add($rules->existsIn(['ruler_id'], 'Rulers'));

        return $rules;
    }
}
