<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CdliTags Model
 *
 * @property \App\Model\Table\AgadeMailsTable|\Cake\ORM\Association\BelongsToMany $AgadeMails
 *
 * @method \App\Model\Entity\CdliTag get($primaryKey, $options = [])
 * @method \App\Model\Entity\CdliTag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CdliTag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CdliTag|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTag|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CdliTag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CdliTag findOrCreate($search, callable $callback = null, $options = [])
 */
class CdliTagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cdli_tags');
        $this->setDisplayField('cdli_tag');
        $this->setPrimaryKey('id');

        $this->belongsToMany('AgadeMails', [
            'foreignKey' => 'cdli_tag_id',
            'targetForeignKey' => 'agade_mail_id',
            'joinTable' => 'agade_mails_cdli_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('cdli_tag')
            ->maxLength('cdli_tag', 100)
            ->allowEmpty('cdli_tag');

        return $validator;
    }
}
