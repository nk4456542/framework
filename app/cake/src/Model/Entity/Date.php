<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Date Entity
 *
 * @property int $id
 * @property string|null $day_no
 * @property string|null $day_remarks
 * @property int|null $month_id
 * @property bool|null $is_uncertain
 * @property string|null $month_no
 * @property int|null $year_id
 * @property int|null $dynasty_id
 * @property int|null $ruler_id
 * @property string|null $absolute_year
 *
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Year $year
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Ruler $ruler
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Date extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'day_no' => true,
        'day_remarks' => true,
        'month_id' => true,
        'is_uncertain' => true,
        'month_no' => true,
        'year_id' => true,
        'dynasty_id' => true,
        'ruler_id' => true,
        'absolute_year' => true,
        'month' => true,
        'year' => true,
        'dynasty' => true,
        'ruler' => true,
        'artifacts' => true
    ];

    private function getDate()
    {
        $parts = array_filter([
            empty($this->ruler) ? null : $this->ruler->ruler,
            empty($this->year) ? null : $this->year->year_no,
            $this->month_no,
            $this->day_no
        ], function ($part) {
            return $part != null && $part != '' && $part != '00' && $part != '--';
        });

        return implode('.', $parts);
    }

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E52_Time-Span',
            'crm:P78_is_identified_by' => [
                '@type' => 'crm:E49_Time_Appellation',
                'rdfs:label' => $this->getDate()
            ],
            'crm:P86_falls_within' => [
                '@type' => 'crm:E52_Time-Span',
                'crm:P4i_is_time-span_of' => self::getEntity($this->ruler)
            ],
            'crm:P67i_is_referred_to_by' => !is_array($this->artifacts) ? [] : array_merge(
                ...array_map(function ($artifact) {
                    return self::getEntities($artifact->inscriptions);
                }, $this->artifacts)
            )
            // TODO:
            //   - day_remarks, is_uncertain
            //   - month_id, year_id → unnecessary given month_no and year_no?
            //   - absolute_year → big if true
        ];
    }
}
