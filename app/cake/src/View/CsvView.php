<?php
namespace App\View;

use CsvView\View\CsvView as CsvView_;

class CsvView extends CsvView_
{
    use TableTrait;

    protected $_responseType = 'csv';

    protected function _serialize()
    {
        $data = $this->_rowsToSerialize($this->viewVars['_serialize']);
        $this->set([
            'table' => $this->prepareTableData($data),
            '_header' => $this->prepareTableHeader($data),
            '_serialize' => 'table'
        ]);
        return parent::_serialize();
    }
}
