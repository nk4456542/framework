<?php
namespace App\View\Helper;

use Cake\View\Helper;

class DropdownHelper extends Helper
{
    public function open($name)
    {
        return implode("\n", [
            "<div class='no-js-dd'>",
            "<button>".$name."</button>",
            "<div class='no-js-dd-content'>"
        ]);
    }

    public function close()
    {
        return '</div></div>';
    }
}
