<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SignReadingsCommentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SignReadingsCommentsTable Test Case
 */
class SignReadingsCommentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SignReadingsCommentsTable
     */
    protected $SignReadingsComments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SignReadingsComments',
        'app.Authors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SignReadingsComments') ? [] : ['className' => SignReadingsCommentsTable::class];
        $this->SignReadingsComments = TableRegistry::getTableLocator()->get('SignReadingsComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SignReadingsComments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
