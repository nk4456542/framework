const { plugins } = require('@citation-js/core')
const articles = require('./article')
const artifacts = require('./artifact')
const publications = require('./publication')

plugins.add('@cdli', {
    input: {
        '@cdli/article': {
            parse (article) {
                return articles.convertToTarget(article)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: 'article_type',
                    value (type) {
                        return ['cdlj', 'cdlb', 'cdlp', 'cdln'].includes(type)
                    }
                }
            }
        },
        '@cdli/artifact': {
            parse (artifact) {
                return artifacts.convertToTarget(artifact)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['designation', 'inscriptions']
                }
            }
        },
        '@cdli/publication': {
            parse (publication) {
                return publications.convertToTarget(publication)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['bibtexkey', 'entry_type_id']
                }
            }
        }
    }
})
