# Citation.js server

## API docs

### POST `/format/:format`

Parse input data and output to a format of your choosing.

  - POST body: input data
  - `:format`: output format (see below)
  - Query params: format options

Example queries:

  - `/format/bibliography?template=chicago&format=html`: get formatted HTML bibliography
    in Chicago style
  - `/format/bibtex`: get BibTeX file

### GET `/formats/input`

Get a JSON array of accepted input formats. A summary:

| Format | Type ID | Description |
|--------|---------|-------------|
| JSON file | @else/json |  |
| BibTeX file | @bibtex/text |  |
| BibTeX data | @bibtex/object | Usually in JSON format |
| Bib.TXT file | @bibtxt/text |  |
| RIS file | @ris/file |  |
| Single RIS record | @ris/record | Usually in JSON format |
| CSL-JSON object | @csl/object |   |
| CSL-JSON array | @csl/list+object |  |
| Array | @else/list+object | Each item is parsed individually |

### GET `/formats/output`

Get a JSON array of accepted output formats. A summary:

| Format | Type ID | Description |
|--------|---------|-------------|
| JSON | data |  |
| ND-JSON | ndjson |  |
| Single label | label | Format: author2020title |
| BibTeX | bibtex |  |
| Bib.TXT | bibtxt |  |
| Formatted bibliography | bibliography | Using CSL; options see below |
| Formatted citation | citation | Using CSL; options see below |
| RIS | ris |  |

#### CSL Formatting options

General options:

| Option | Description |
|--------|-------------|
| template | Style ID: `chicago`, `apa`*, `vancouver` |
| lang | Lang ID: `en-US`*, `de-DE`, `nl-NL`, `fr-FR`, `es-ES` |
| format | Formatting ID: `html`, `text`*, `rtf` |

\* default

Bibliography options:

| Option | Description |
|--------|-------------|
| nosort | `true`, `false`* |

\* default

Citation options:

| Option | Description |
|--------|-------------|
| entries | Entry IDs that should be formatted |

\* default

## Development docs

### Install dependencies

    npm install

#### Update dependencies

Check for outdated dependencies:

    npm outdated

Update dependencies with minor/patch version bumps:

    npm update

Update dependencies with major version bumps (fill in `DEPENDENCY` and `VERSION` as appropriate):

    npm install DEPENDENCY@VERSION

#### Audit dependencies

Check for dependencies with security issues:

    npm audit

Fix security issues in dependencies (apart from major version; see above):

    npm audit fix

### Run server

Production:

    npm start

Development (watches files and restarts on change):

    npm run start:dev
