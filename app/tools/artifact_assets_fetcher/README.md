## Artifact Assets Fetcher

Fetches the assets for artifacts using their P code and returns a list of the assets in JSON format. 

Runs fine on Python 2 and Python 3.

### Arguments

```
'-d', '--directory', help='Path to the dl directory'
'-n', '--file_name', default='artifacts.json', help='File name for resulting json file'  
'-bu', '--base_url', default='cdli.ucla', help='URL to be appended to asset files' 
'-x', '--exempted_ids', default=[], help='File with the list of exempted artifact ids'
```

### Usage Example
```
python fetcher.py -d folder/folder/dl -n results.json -bu stackoverflow.com -x exempted.txt
```
In the example above:

 - `folder/folder/dl` is the 'dl' directory, where the sub-directories for the assets are stored. This value **must** be provided.
 
 - `results.json` is the name used to save the list of the assets when fetched. The default value is `artifacts.json`.
 
 - `stackoverflow.com` is the name of the url appended to the file path of the assets, e.g. stackoverflow.com/dl/file.jpg. The default value is `cdli.ucla`.
 
 - `exempted.txt` contains the IDs for the artifacts whose assets shouldn't be fetched. The file should have one ID per line. The default is `[]` which means no ID should be exempted. 


## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE.txt) file for details

## Acknowledgments

* I appreciate [Émilie Pagé-Perron](https://gitlab.com/epageperron) for making my contribution to this project easy.
